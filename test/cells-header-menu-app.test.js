import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../cells-header-menu-app.js';

suite('CellsHeaderMenuApp', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<cells-header-menu-app></cells-header-menu-app>`);
    await el.updateComplete;
    const options = [
      {
        name: 'Proceso de atención',
        icon: 'coronita:productportfolio',
        maxWidthColumn: '250px',
        page: '#',
        items: [
          {
            icon: 'coronita:productportfolio',
            name: 'Modificar casos terminados',
            page: '#',
          },
        ],
      },
      {
        name: 'Configuración',
        icon: 'coronita:settings',
        maxRows: 9,
        maxWidthColumn: '230px',
        page: '#',
        items: [
          {
            icon: 'coronita:productportfolio',
            name: 'Parametros',
            page: '#',
          },
        ],
      },
      {
        name: 'Test',
        items: [
          {
            name: 'test'
          },
        ]
      },
      {
        name: 'Test 2'
      }
    ];
    el.items = options;
  });

  test('Instantiating component', () => {
    const header = el.shadowRoot.querySelector('header');
    assert.isAccessible(header);
  });

  test('Test function readyInit (Click event)', () => {
    window.dispatchEvent(
      new CustomEvent('click', {
        detail: true,
        bubbles: true,
        composed: true,
      })
    );
  });

  test('Test function readyInit (Mouseover event)', () => {
    window.dispatchEvent(
      new CustomEvent('mouseover', {
        detail: true,
        bubbles: true,
        composed: true,
      })
    );
  });

  test('Test function collapsedSidebar', () => {
    const evt = {
      stopPropagation: function () {},
    };
    el.collapsedSidebar(evt);
  });

  test('Test function collapsedSidebar(toggle)', () => {
    const evt = {
      stopPropagation: function () {},
    };
    el.shadowRoot
      .querySelector('.wrapper-aside')
      .classList.toggle('collapsible');
    el.collapsedSidebar(evt);
  });

  test('Test function selectedOptionMenu', () => {
    const evt = {
      stopPropagation: function () {},
    };
    el.selectedOptionMenu(evt, {}, 'option-menu-0', 'list-vertical-submenu-0');
  });

  test('Test function hoverMenu(IN)', () => {
    const evt = {
      stopPropagation: function () {},
    };
    el.hoverMenu(evt, 'in');
  });

  test('Test function hoverMenu(OUT)', () => {
    const evt = {
      stopPropagation: function () {},
    };
    el.hoverMenu(evt, 'out');
  });

  test('Test function goHome', () => {
    el.linkHomePage = '#';
    el.goHome();
  });

  test('Test function showInfoProfile', () => {
    const evt = {
      stopPropagation: function () {},
    };
    el.showInfoProfile(evt);
  });

  test('Test function logOut', () => {
    el.logOut();
  });

  test('Test function goProfile', () => {
    el.goProfile();
  });

  test('Test function imageUser(urlImgProfile)', () => {
    const user = {
      urlImgProfile: 'urlImgProfile'
    }
    el.user = user;
  });

  
  test('Test function imageUser(sexo)', () => {
    const user = {
      sexo: 'F'
    }
    el.user = user;
  });

  test('Test events', () => {
    const element = el.shadowRoot.querySelector('.box-info-user-profile');
    element.dispatchEvent(new CustomEvent('click', { 
      detail: true,
      bubbles: true, 
      composed: true
    }));

    const element2 = el.shadowRoot.querySelector('.item-option-menu');
    element2.dispatchEvent(new CustomEvent('click', { 
      detail: true,
      bubbles: true, 
      composed: true
    }));

    el.viewMode = 'horizontal';
    const element3 = el.shadowRoot.querySelector('.wrapper-aside');
    element3.dispatchEvent(new CustomEvent('mouseover', { 
      detail: true,
      bubbles: true, 
      composed: true
    }));

    const element4 = el.shadowRoot.querySelector('.wrapper-aside');
    element4.dispatchEvent(new CustomEvent('mouseout', { 
      detail: true,
      bubbles: true, 
      composed: true
    }));

    el.theme = 'light';

    const element5 = el.shadowRoot.querySelector('.logo');
    element5.dispatchEvent(new CustomEvent('mouseover', { 
      detail: true,
      bubbles: true, 
      composed: true
    }));
  });

  

});
